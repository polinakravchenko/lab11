﻿// lab11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

void main()
{
	int size = 0;

	cout << "Enter array size: " << endl;
	cin >> size; 

	int* arr = new int[size];

	for (int i = 0; i < size; i++)
	{
		arr[i] = rand() % 5;
	}
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << "\t";
		cout << arr + i << endl;
	}

	delete [] arr;
}